##
# Fix Colorspace Issue
# You can use one of following values:
#   "rgba", "abgr", "argb", "bgra"

ini_set("force_colorspace","rgba");

##
# Initializing Information
#

ini_set("rom_name",             "SuperMan-Rom™ ");
ini_set("rom_version",          "V2.9.0");
ini_set("rom_author",           "Tkkg1994");
ini_set("rom_device",           "SM-G93XF/FD/K/L/S/W8");
ini_set("rom_date",             "12.02.2017");

##
# Show Animated Splash
#
anisplash(
#  #-- Number of Loop
    1,
  
  #-- Frame  [ Image, duration in millisecond ]
    "anim/a00", 0,
    "anim/a01", 0,
    "anim/a02", 0,
    "anim/a03", 0,
    "anim/a04", 0,
	"anim/a05", 0,
	"anim/a06", 0,
	"anim/a07", 0,
	"anim/a08", 0,
	"anim/a09", 0,
	"anim/a10", 1,
	"anim/a11", 1,
	"anim/a12", 1,
	"anim/a13", 1,
	"anim/a14", 1,
	"anim/a15", 1,
	"anim/a16", 1,
	"anim/a17", 1,
	"anim/a18", 1,
	"anim/a19", 1,
	"anim/a20", 1,
	"anim/a21", 1,
	"anim/a22", 1,
	"anim/a23", 1,
	"anim/a24", 1500
);

##
# Font and Theme Selection
#

fontresload( "0", "ttf/Roboto-Regular.ttf;ttf/DroidSansArabic.ttf;ttf/DroidSansFallback.ttf;", "12" ); #-- Use sets of font (Font Family)
theme("superman");

##
# SET LANGUAGE & FONT FAMILY
#

loadlang("langs/en.lang");
fontresload( "0", "ttf/Roboto-Regular.ttf", "12" ); #-- "0" = Small Font ( Look at Fonts & UNICODE Demo Below )
fontresload( "1", "ttf/Roboto-Regular.ttf", "18" ); #-- "1" = Big Font

##
#   SHOW ROM/Mod INFORMATION
#
viewbox(
	#-- Title
	"<~welcome.title>",

 	#-- Text
	"<~welcome.text1> <b>"+
	#-- Get Config Value
	ini_get("rom_name")+
	"</b> <~common.for> <b>"+ini_get("rom_device")+"</b>.\n\n"+

	"<~welcome.text2>\n\n"+
	
	"  <~welcome.version>\t: <b><#selectbg_g>"+ini_get("rom_version")+"</#></b>\n"+
	"  <~welcome.updated>\t: <b><#selectbg_g>"+ini_get("rom_date")+"</#></b>\n\n\n"+

	"<~welcome.next>",

	#-- Icon
	"@welcome"
);

##
# LICENSE
#

agreebox(
	#-- Title
	"SuperMan-Rom™",

	#-- Subtitle / Description
	"Read Carefully",

	#-- Icon:
	"@license",

	#-- Text Content 
	resread("license.txt"),

	#-- Checkbox Text
	"Have you really read it and are agree?",

	#-- Unchecked Alert Message
	"You've to agree!"
);

##
# MAIN MENU- INSTALLER n MISC
#
menubox(
	#-- Title
	"SuperMan-Rom™ Menu",

	#-- Sub Title
	"Please select from the Menu Below to Modify the required features",

	#-- Icon
	"@apps",
 
	#-- Will be saved in /tmp/aroma/menu.prop
	"menu.prop",
    
     #-------------------------+-----------------[ Menubox Items ]-------------------------+---------------#
     # TITLE                   |  SUBTITLE                                                 |   Item Icons  #
     #-------------------------+-----------------------------------------------------------+---------------#	
	
	"Installation",		"ROM Installation with Various Features - RECOMMENDED",		"@install",      #-- selected = 1
	"System Info",		"Get and show device/partition informations",			"@info",         #-- selected = 2
	"ChangeLog",		"SuperMan ChangeLog",						"@agreement",    #-- selected = 3
	"Quit Install",		"Leaving the Aroma setup to install another Rom :(",		"@install2"       #-- selected = 4

);

##
# System Info 
#

if prop("menu.prop","selected")=="2" then

	#-- Show Please Wait
	pleasewait("Getting System Information...");

	#-- Fetch System Information
	setvar(
		#-- Variable Name
		"sysinfo",

		#-- Variable Value
		"<@center><b>Your Device System Information</b></@>\n\n"+

		"Device Name\t\t: <#ff0000>Galaxy S7/S7 edge</#>\n"+
		"Board Name\t\t: <#ff0000>SM-G930F/G935F</#>\n"+
		"Manufacturer\t\t: <#ff0000>Samsung</#>\n"+
	  
		"\n"+

		"System Size\t\t: <b><#selectbg_g>"+getdisksize("/system","m")+" MB</#></b>\n"+
		"\tFree\t\t: <b><#selectbg_g>"+getdiskfree("/system","m")+" MB</#></b>\n\n"+
		"Data Size\t\t: <b><#selectbg_g>"+getdisksize("/data","m")+" MB</#></b>\n"+
		"\tFree\t\t: <b><#selectbg_g>"+getdiskfree("/data","m")+" MB</#></b>\n\n"+
		"SDCard Size\t\t: <b><#selectbg_g>"+getdisksize("/sdcard","m")+" MB</#></b>\n"+
		"\tFree\t\t: <b><#selectbg_g>"+getdiskfree("/sdcard","m")+" MB</#></b>\n\n"+

		""
	);
  
	#-- Show Textbox
	textbox(
		#-- Title
		"System Information",
    
		#-- Subtitle
		"Current system Information on your S7",
    
		#-- Icon
		"@info",
    
		#-- Text
		getvar("sysinfo")
	);
  
	#-- Show Alert
	alert(
		#-- Alert Title
		"Finished",
    
		#-- Alert Text
		"You will be back to Menu",
    
		#-- Alert Icon
		"@alert"
	);
  
	#-- Back to Menu ( 2 Wizard UI to Back )
	back("2");
  
endif;

##
# CHANGELOG DISPLAY
#

if prop("menu.prop","selected")=="3" then

	#-- TextDialog 
	textdialog(
		#-- Title
		"SuperMan-Rom Changelog",
		#-- Text
		resread("changelog.txt"),
		#-- Custom OK Button Text (Optional)
		"Close"
	);
 
	#-- Back to Menu ( 2 Wizard UI to Back )
	back("1");
  
endif;

##
# QUIT INSTALLER
#

if prop("menu.prop","selected")=="4" then

#-- Exit
	if confirm(
		#-- Title
		"Exit",
		#-- Text
		"Are you sure want to exit the Installer?",
		#-- Icon (Optional)
		"@alert"
	)=="yes"
	then
		#-- Exit 
		exit("");
	endif;
endif;


##
#  Select Type of Install
#

if prop("menu.prop","selected")=="1" then

# Write tmp file for restore check
writetmpfile("restore.prop","restore=no");
writetmpfile("jumpinstall.prop","install=no");

# Check if wipe.sh is backed up from last time
resexec("checkconfig/checkconfig.sh");

if prop("restore.prop","restore")=="yes" then

	if confirm(
			#-- Title
			"Backup found!",
			#-- Text
			"Do you want to restore your settings from last aroma installation?",
			#-- Icon (Optional)
			"@install",
			#-- Yes Text
			"Yes, restore backup.",
			#-- No Text
			"No, start fresh."
	)=="yes"
		then
			resexec("checkconfig/restoreconfig.sh");
			writetmpfile("jumpinstall.prop","install=yes");
			alert("Result","Backup restored");
		else
			writetmpfile("jumpinstall.prop","install=no");
			alert("Result","Backup not restored, going on with fresh installation");
	endif;
	if prop("jumpinstall.prop","install")=="yes" then
		if confirm(
				#-- Title
				"Jump to installation screen?",
				#-- Text
				"Do you want to jump directly to install screen? Since you already got an aroma backup.",
				#-- Icon (Optional)
				"@install",
				#-- Yes Text
				"Yes, keep all settings.",
				#-- No Text
				"No, change settings."
		)=="yes"
			then
				writetmpfile("jumpinstall.prop","install=yes");
			else
				writetmpfile("jumpinstall.prop","install=no");
		endif;
	endif;
endif;

if prop("jumpinstall.prop","install")=="no" then

##
# Wipe Form
#

form(
    "Wipe",
    "Choose your wipe option here",
    "icons/install2",
    "wipe.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"wipe",		"Wipe method",		"",									"group",
	"full",		"Full Wipe",		"Wipes everything except internal and external SD card",		"select.selected",
	"clean",	"Clean Install",	"Wipes everything except external SD card",				"select",
	"dirty",	"Dirty Flash",		"Wipes only system (there will be bugs with this). Can also be used to change settings after clean install!",			"select"
);

##
# Kernel Form
#

form(
    "Kernel/Recovery",
    "Choose your desired kernel here. You can also update recovery if you want",
    "icons/install2",
    "kernel.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"kernel",	"Kernel Selection",		"",									"group",
	"stock",	"Stock Kernel",			"This is just a full stock kernel as you can find on stock rom",	"select",
	"super",	"SuperKernel",			"SuperKernel V2.11.0 with many tweaks",					"select",
	"superstock",	"SuperStock",			"Latest SuperStock V2.11.0 for our galaxy s7",				"select.selected",
	"control",	"Kernel Control app",		"",									"group",
	"synapse",	"Synapse",			"Installs Synapse app to control the Kernel. Only working on SuperKernel",	"check",
	"adiutor",	"Kernel Adiutor",		"Installs latest Kernel Adiutor app. You can control all kernels with it",	"check",
	"recovery",	"Recovery Update",		"",									"group",
	"update",	"Update Recovery",		"This will update your TWRP to latest official 3.2.1-1",		"select",
	"keep",		"Keep current Recovery",	"Will keep your current installed TWRP version",		"select.selected"
);

##
# buildtweaks.prop Form
#

form(
    "Build.prop and init.d Tweaks",
    "You can choose if you want to install any build.prop or init.d tweaks",
    "icons/install2",
    "buildtweaks.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"build",	"Select build.prop tweaks",	"",											"group",
	"finger",	"Fingerprint after reboot",	"Enables fingerprint right after a reboot without entering your password first (only on modded SystemUI)",	"check.checked",
	"user",		"Multiuser",			"Enable Multi User in settings and in statusbar",					"check.checked",
	"fix",		"Screen mirror fix",		"Applies a screen mirror fix (was present all the time before)",			"check.checked",
	"softkey",	"Software Keys (Navbar)",	"Enable S8 sofware keys (this is only a build.prop mod)",				"check"
);

alert("Important:", "Snapchat can work with root, if you select Magisk V15.3 below! Follow the steps provided on XDA to get Safetynet working.");

##
# Magisk Form
#

form(
    "Root/Magisk",
    "You can choose many things here, read carefully:",
    "icons/install2",
    "magisk.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"magisk",	"Magisk V15.3",		"",									"group",
	"yes",		"Yes",			"Magisk V15.3 will be installed with MagiskSU",				"select.selected",
	"no",		"No",			"Won't install Magisk",							"select",
	"root",		"Root method", 		"",									"group",
	"su",		"SuperSU 2.82-SR5",	"Will install SuperSU 2.82 SR5",					"select",
	"magisk",	"MagiskSU",		"Installs MagiskSU based on phh root",					"select.selected",
	"phh",		"Phh's r310",		"Installs normal r310 phh superuser",					"select",
	"no",		"No additional root",	"I don't want any root method to be installed",				"select",
	"busybox",	"Busybox", 		"",									"group",
	"osm",		"Busybox by osmosis",	"Installs busybox, needed for RomControl",				"select.selected",
	"yds",		"Busybox by YashdSaraf","Installs busybox, needed for RomControl",				"select",
	"no",		"No busybox",		"Don't install busybox",						"select",
	"xposed",	"Xposed", 		"",									"group",
	"xpsma",	"Magisk module 89.2",	"Installs Magisk version of Xposed, safetynet will FAIL!!",		"select",
	"no",		"No Xposed",		"Recommend right now, many things neeed to be tested on xposed (can give bugs)",	"select.selected"
);

if (prop("magisk.prop","xposed")=="ofs" || prop("magisk.prop","xposed")=="xpsma") then
alert("Attention:", "You selected Xposed during the install, please be aware that this will break safetynet, probably kill romcontrol mods and slows down the system/more battery drain. Also I won't support Xposed related questions in the threads on XDA/GrifoDev!");
endif;

##
# Sound Form
#

form(
    "Dual Speaker/Camera Mod",
    "Choose to install dual speaker mod and camera mod",
    "icons/install2",
    "sound.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"sound",	"Sound Level",			"",								"group",
	"mod",		"Dual Speaker mod",		"Installs latest dual speaker mod",				"select",
	"stock",	"Use stock sound",		"Will keep the stock sound files",				"select.selected",
	"camera",	"Camera Mod",			"",								"group",
	"mod",		"Modded camera",		"Installs modded camera by zeroprobe with advanced quality",	"select",
	"stock",	"Stock camera",			"Will keep the stock camera files",				"select.selected"
);

##
# Emoji Form
#

form(
    "Emojis",
    "Do you want to install the iOS, oreo or stock Emojis in keyboard?",
    "icons/install2",
    "emoji.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"emoji",	"Select which Icons you want",	"",								"group",
	"ios",		"Install iOS 10.2 emojis",	"Will install new iOS emojis in samsung keyboard",		"select",
	"oreo",		"Install oreo emojis",		"Will install the new oreo emojis for the samsung keyboard",	"select",
	"stock",	"Install stock emoji",		"Will display stock emojis in samsung keyboard",		"select.selected"
);

##
# SystemUI/Settings Form
#

form(
    "SystemUI and Settings",
    "You can choose here if you want to install modded systemUI and settings",
    "icons/install2",
    "mods.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"systemUI",	"Choose your prefered SystemUI",		"",									"group",
	"modded",	"Modded SystemUI",		"Includes all romcontrol features, disabled sd card notificaitons, fingerprint after reboot mode and many more features.",		"select.selected",
	"stock",	"Stock SystemUI",		"No features included in here. Just stock SystemUI as you can find on any rom",		"select",
	"settings",	"Choose your prefered Settings",		"",									"group",
	"modded",	"Modded Settings",		"SuperMan logo and touchkey light duration enabled",					"select.selected",
	"stock",	"Stock Settings",		"No special features, all stock in this settings",					"select"
);

##
# Launcher Form
#

form(
    "Launcher",
    "Choose between stock and modded launcher",
    "icons/install2",
    "launcher.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"launcher",	"Launcher", 		"",									"group",
	"stock",	"Stock Launcher",	"Normal touchwiz launcher as always",					"select",
	"modded",	"Modded Launcher",	"Touchwiz Page Transition effects, TWSwipe with will replace Filpboard",	"select.selected"
);

##
# Modem Form
#

form(
    "Modem and Bootloader",
    "You want to auto update your modem and bootloader?",
    "icons/install2",
    "cpandbl.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"modem",	"Modem update F/FD/W8",		"",										"group",
	"update",	"Update Modem",			"Will auto update your modem on F/FD/W8",					"select.selected",
	"keep",		"Keep current Modem",		"Will keep your current modem and change nothing",				"select",
	"bootloader",	"Bootloader update F/FD/W8",	"",										"group",
	"update",	"Update Bootloader",		"Will auto update your BL on F/FD/W8 models. Phone will auto reboot!",		"select.selected",
	"keep",		"Keep current Bootloader",	"Will keep your current bootloader and change nothing",				"select"
);

##
# Bootanimation Form
#

form(
    "Splash and Bootanim",
    "Choose Splash screen and boot animation here",
    "icons/install2",
    "bootanim.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"anim",		"Bootanimations",			"",											"group",
	"stock",	"Stock animation",		"Just as always, stock boot/shutdown animation",						"select",
	"super_black",	"SuperMan black animation",	"New SuperMan bootanimation with black background, all credits to myellow (he's awesome)",	"select.selected",
	"super_white",	"SuperMan white animation",	"New SuperMan bootanimation with white background, credits to myellow (he's still awesome)",	"select",
	"super_black3d","SuperMan black 3D animation",	"Even a little bit more updated superman with a 3D animation with black background",		"select",
	"super_white3d","SuperMan white 3D animation",	"Even a little bit more updated superman with a 3D animation with white background",		"select",
	"splash",	"Splash Screen",		"",												"group",
	"stock",	"Stock Splash",			"Stock S7 splash screen will be installed",							"select",
	"super_black",	"SuperMan black Splash",	"New SuperMan black Splash will be installed",							"select",
	"nothing",	"Keep current",			"Keeps your current splash screen (no matter which it is)",					"select.selected"
);

##
# AdAway Form
#

form(
    "AdAway",
    "Select if you want adblocker by default or not",
    "icons/install2",
    "adaway.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"adblocker",	"Adblocker",		"",											"group",
	"enable",	"Enable by default",	"Enables Adblocker by default (3minit battery needs disabled adblocker to work)",	"select.selected",
	"disable",	"Disable by default",	"Disables Adblocker by default. However you can still enable it in the app later",	"select"
);

alert("Attention", "Removing samsung apps can lead to force closes and not working features!");

##
# Sub Checkbox 1
#

checkbox(
  #-- Title
	"Bloat Samsung Options",
	  
  #-- Sub Title
    "Everything what is ticked will be installed!",
	  
  #-- Icon:
     "icons/install2",
	 
  #-- Will be saved in /tmp/aroma/samsung.prop
	"samsung.prop",

	  #----------------------------------[ Selectbox With Groups ]-----------------------------------#
	  # TITLE            |  SUBTITLE                                                 | Initial Value #
	  #------------------+-----------------------------------------------------------+---------------#  

	"Samsung Apps",		"",									2,	#-- group 1
	"Allshare",		"Allshare Cast by Samsung",						1,	#-- item.1.1
	"ANT",			"ANT-Hal and Plugins",							1,	#-- item.1.2
	"Memo",			"Memo App from Samsung",						1,	#-- item.1.3
	"Mirrorlink",		"",									1,	#-- item.1.4
	"SBrowser",		"Samsungs own Browser",							1,	#-- item.1.5
	"Samsung Calendar",	"Planner App and Widget",						1,	#-- item.1.6
	"Video Editor",		"",									1,	#-- item.1.7
	"Weather",		"Weather Widget",							1,	#-- item.1.8
	"Calculator",		"",									1,	#-- item.1.9
	"Email",		"Samsungs default Mail program",					1,	#-- item.1.10
	"Galaxy Store",		"Samsungs own Galaxy App",						1,	#-- item.1.11
	"Gallery",		"Samsungs Gallery App",							1,	#-- item.1.12
	"Game",			"Samsungs Game Centre",							1,	#-- item.1.13
	"MyFiles",		"Myfiles Application",							1,	#-- item.1.14
	"SHealth",		"Tracks fitness and more",						1,	#-- item.1.15
	"SVoice",		"Samsung Voice Assistant",						1,	#-- item.1.16
	"Themestore",		"",									1,	#-- item.1.17
	"VoiceNote",		"",									1,	#-- item.1.18
	"SmartSwitch",		"",									0,	#-- item.1.19
	"Gear S manager", 	"Used for Gear S, Gear S2 etc",						1,	#-- item.1.20
	"Gear VR manager", 	"You have to install this in order to use your oculus gear VR",		1,	#-- item.1.21
	"Microsoft Excel",	"",									0,	#-- item.1.22
	"Microsoft PowerPoint",	"",									0,	#-- item.1.23
	"Microsoft Word",	"",									0,	#-- item.1.24
	"Knox",			"For secure folder, samsung pay and my knox to work",			0,	#-- item.1.25
	"Samsung Music Player",	"",									1,	#-- item.1.26
	"Samsung OneDrive",	"",									0,	#-- item.1.27
	"Samsung Notes",	"",									0,	#-- item.1.28
	"Samsung Video Player",	"",									0,	#-- item.1.29
	"Samsung Gear 360 viewer",	"",								0,	#-- item.1.30
	"Additional Fonts",	"Will install a font apk with many custom fonts in it",			0,	#-- item.1.31
	"Edge Panel",		"Installs needed apps for the edge panel",				1	#-- item.1.32

	  #--------[ Initial Value = 0: Unselected, 1: Selected, 2: Group Item, 3: Not Visible ]---------#
);

##
# Sub Checkbox 2
#

checkbox(
  #-- Title
	"Bloat Google Options",
	  
  #-- Sub Title
    "Everything what is ticked will be installed!",
	  
  #-- Icon:
     "icons/install2",
	 
  #-- Will be saved in /tmp/aroma/google.prop
	"google.prop",

	  #----------------------------------[ Selectbox With Groups ]-----------------------------------#
	  # TITLE            |  SUBTITLE                                                 | Initial Value #
	  #------------------+-----------------------------------------------------------+---------------#  

	"Google Apps",		"",									2,	#-- group 1
	"Google Now",		"",									0,	#-- item.1.1
	"Google Chrome",	"",									0,	#-- item.1.2
	"Google Drive",		"",									0,	#-- item.1.3
	"Gmail",		"",									0,	#-- item.1.4
	"Hangouts",		"",									0,	#-- item.1.5
	"Google Maps",		"",									0,	#-- item.1.6
	"Play Music",		"",									0,	#-- item.1.7
	"Google Movies",	"",									0,	#-- item.1.8
	"Youtube",		"",									0,	#-- item.1.9
	"AndroidPay",		"",									0,	#-- item.1.10
	"Google Photos",	"",									0,	#-- item.1.11
	"Google Assistant",	"",									0	#-- item.1.12

	  #--------[ Initial Value = 0: Unselected, 1: Selected, 2: Group Item, 3: Not Visible ]---------#
);

##
# Sub Checkbox 3
#

checkbox(
  #-- Title
	"Useful Apps",
	  
  #-- Sub Title
    "Everything what is ticked will be installed!",
	  
  #-- Icon:
     "icons/install2",
	 
  #-- Will be saved in /tmp/aroma/others.prop
	"others.prop",

	  #----------------------------------[ Selectbox With Groups ]-----------------------------------#
	  # TITLE            |  SUBTITLE                                                 | Initial Value #
	  #------------------+-----------------------------------------------------------+---------------#  

	"General Apps",		"",									2,	#-- group 1
	"AdAway",		"To enable and disable adblocking",					1,	#-- item.1.1
	"OneNote",		"",									0,	#-- item.1.2
	"SuperControl Free",	"To control all rom settings, if you don't install it you can't adjust anything!",	1,	#-- item.1.3
	"GBInstaPlus+",		"Modded Instagram to download images and use custom themes",		0,	#-- item.1.4
	"GBWhatsApp+",		"Modded WhatsApp which can be fully themed",				0,	#-- item.1.5
	"Modded Youtube",	"Play Youtube Videos in the Background",				0	#-- item.1.6

	  #--------[ Initial Value = 0: Unselected, 1: Selected, 2: Group Item, 3: Not Visible ]---------#
);

##
# CSC Form
#

form(
    "CSC",
    "You can choose if you want to include default csc or flash your csc after rom",
    "icons/install2",
    "csc.prop",
  #
  # Type:
  #  - group              = Group
  #  - select             = Select Item
  #  - select.selected    = Selected Select Item
  #  - check              = Checkbox Item
  #  - check.checked      = Checked Checkbox Item
  #  - hide               = Hidden
  #
  #-------------+-----------------------[ Selectbox Without Group ]------------------------------#
  # PROP ID     | TITLE            |  SUBTITLE                                   |    Type       #
  #-------------+--------+-------------------------------------------------------+---------------#
	"CSC",		"Europe Multi-CSC",	"",							"group",
	"BTU",		"BTU",			"United Kingdom unbranded (default)",			"select.selected",
	"CPW",		"CPW",			"United Kingdom (unknown)",				"select",
	"DBT",		"DBT",			"Germany unbranded",					"select",
	"ITV",		"ITV",			"Italian unbranded",					"select",
	"KOR",		"KOR",			"Korean unbranded",					"select",
	"NEE",		"NEE",			"Nordic Countries unbranded",				"select",
	"XEF",		"XEF",			"France unbranded",					"select",
	"XEO",		"XEO",			"Poland unbranded",					"select",
	"XEU",		"XEU",			"United Kingdom O2 UK branded",				"select"
);

# This endif comes from the jump command
endif;

# Installation UI

ini_set("text_next", "Install Now");
ini_set("icon_next", "@installbutton");
  
viewbox(
	#-- Title
	"Ready to Install",

	#-- Text
	"SuperMan-Rom is ready to be installed.\n\n"+
	"Press <b>Install ROM</b> to begin the installation.\n\n"+
	"To review or change any of your installation settings, press <b>Back</b>.\n\n"+
	"Press Menu -> Quit Installation to quit.",

	#-- Icon
	"@install"
);

endif;

##
# INSTALLATION PROCESS
#

if prop("menu.prop","selected")== "1" then

ini_set("text_next", "Next");
ini_set("icon_next", "@next");

install(
	"SuperMan-Rom™ Installation",
	getvar("rom_name") + "\n" +
	"Please wait while SuperMan-Rom is conquering your phone" +
	"",
	"icons/install2"
);

ini_set("text_next", "Finish");
ini_set("icon_next", "@finish");
endif;
